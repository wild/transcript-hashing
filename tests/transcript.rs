#![forbid(unsafe_code)]

use transcript_hashing::Blake3Transcript;

#[test]
fn witness_appends() {
    let mut ts1 = Blake3Transcript::new_deterministic(b"witness appends");
    let mut ts2 = Blake3Transcript::new_deterministic(b"witness appends");

    assert_eq!(ts1.challenge::<u64>(b"a"), ts2.challenge::<u64>(b"a"));
    assert_eq!(ts1.random::<u64>(), ts2.random::<u64>());

    // Appending to one transcript but not the other will diverge.
    ts1.append(b"Hello world".as_ref());

    assert_ne!(ts1.challenge::<u64>(b"b"), ts2.challenge::<u64>(b"b"));
    assert_ne!(ts1.random::<u64>(), ts2.random::<u64>());

    // Appending the same message to the other will not converge if
    // interleaved with challenges.
    ts2.append(b"Hello world".as_ref());

    assert_ne!(ts1.challenge::<u64>(b"c"), ts2.challenge::<u64>(b"c"));
    assert_ne!(ts1.random::<u64>(), ts2.random::<u64>());
}
