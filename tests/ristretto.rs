#![cfg(feature = "curve25519-dalek")]
#![forbid(unsafe_code)]
#![allow(non_snake_case)]

use transcript_hashing::Blake3Transcript;

#[test]
fn check() {
    use curve25519_dalek::{ristretto::RistrettoPoint, scalar::Scalar};

    let mut ts = Blake3Transcript::new_deterministic(b"Ristretto is awesome!");

    if ts.append(1 == 1) {
        let _chal: RistrettoPoint = ts.challenge(b"chal");
        let _nonce: Scalar = ts.random();
    }
}
